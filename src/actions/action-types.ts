export enum actionTypes {
    setContacts = 'SET_CONTACTS',
    getContacts = 'GET_CONTACTS',
    deleteContact = 'DELETE_CONTACT',
    updateContact = 'UPDATE_CONTACT',
    updateContactSuccess = 'UPDATE_CONTACT_SUCCESS'
}
